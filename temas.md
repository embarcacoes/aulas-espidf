# Temas de aula

**Nota**: Cada tópico não necessariamente corresponde a uma aula exclusiva, pois muitas aulas podem abranger vários tópicos.

# Temas Abordados

- Eletricidade básica
- Protoboard e circuitos simples
- Instrumentação eletrônica
- Circuitos básicos com sensores
- Básico de Transistores
- Como Funciona um Circuito Digital
- O Que São Microcontroladores
- Devmodule vs Module vs Microcontrolador
- História do ESP32
- Resumo do IDF e da Extensão Pro VSCode
- Escrita Digital
- Leitura Digital

## Falta

- Configurações Extras GPIO
- Interrupção - opcional
- ESP_LOGI
- PWM
- ADC
- DAC (Opcional)
- UART (Opcional)
- SPI (Opcional, mas seria massa)
- I2C (Opcional)
- Modulação no ESP-IDF/Bibliotecas
- Menuconfig (Opcional)
- Wi-Fi
- MQTT
- Armazenamento NVS e SPI (Opcional)
- FreeRTOS - Tasks
- FreeRTOS - Filas
- FreeRTOS - Semaforo
- Webserver (Opcional)
- Bluetooth (Opcional)

## Prático

- Semaforo
- Genius
- LED Fade
- Controlar Servo Motor
- Controlar Motor de Passo
- Utilizando um LDR
- Tocando a Música do Mario
- Comunicando 2 ESPs via UART
- Usando BME/BMP280 com ESP32
- Usando Módulo LoRa com ESP-IDF
- Criando Scanner de Redes
- Enviar Temperatura via MQTT
- Salvar Credenciais na NVS
- Salvar Dados na Flash
- Criar um Sistema Multitask
- Criar um Servidor Web com ESP32
- Carrinho Bluetooth com ESP32
