#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "hal/gpio_types.h"
#include <stdio.h>

void app_main(void) {
    printf("Ola mundo\n");

    gpio_set_direction(GPIO_NUM_5,
                       GPIO_MODE_OUTPUT); // Configuro o pino 5 como saida

    while (true) { // Repito infinitamente

        gpio_set_level(GPIO_NUM_5, 1); // Aciono o pino 5

        vTaskDelay(2000 / portTICK_PERIOD_MS); // Espero 2 segundos

        gpio_set_level(GPIO_NUM_5, 0); // Desativo o pino 5

        vTaskDelay(2000 / portTICK_PERIOD_MS); // Espero 2 segundos
    }
}
