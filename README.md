# Aulas ESPIDF

Respositorio com os exemplos dados durante as aulas sobre ESPIDF

## Comandos uteis:

- Exportar ambiente de desenvolvimento

```bash
get_idf
```

- Criar projeto

```bash
idf.py create-project <nome do projeto>
```

- Configurar placa

```bash
idf.py set-target <nome da placa>
```

exemplo:

```bash
idf.py set-target esp32s2
```

atualmente as possiveis targets são possiveis:

```
esp32 esp32s2 esp32c3 esp32s3 esp32c2 esp32c6 esp32h2
```

- Buildar projeto

```bash
idf.py build
```

- Gravar projeto na placa

```bash
idf.py flash
```

- Monitor Serial

```bash
idf.py monitor
```

- Fazer os 3 passos acima um após o outro

```bash
idf.py build flash monitor
```

- iniciar servidor OpenOCD

```bash
openocd -f board/<board-config>.cfg
```

exemplo:

```bash
openocd -f board/esp32s3-builtin.cfg
```

nota: ao exportar o ambiente de desenvolvimento as configs do openocd são adicionadas ao path.

- Iniciar GDB

```bash
idf.py gdb
```

- Iniciar GDBTui

```bash
idf.py gdbtui
```

Nota: tanto o GDB quanto o GDBTui necessitam de uma instancia do OpenOCD em execução para funcionar

## Exemplos existentes

- hello world
- blink
- acionar led com botão

## Materiais recomendados

- [e-book Explorando o Potencial do ESP32: Guia de Iniciação ao ESP-IDF 5](https://embarcados.com.br/ebook/e-book-explorando-o-potencial-do-esp32-guia-de-iniciacao-ao-esp-idf-5/)
- [Introdução ao ESPIDF](https://www.youtube.com/watch?v=7-hJGuJMrY0)
